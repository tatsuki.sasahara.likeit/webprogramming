<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
	<head>
    	<meta charset="UTF-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ログイン</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link href="css/login.css" rel="stylesheet" />

	</head>
	<body>
    <br>
    <div class="top">
    	<h1>ログイン画面</h1>
    </div>
    <br>
    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
    <br>
    <form class="form-signin" action="LoginServlet" method="post">
    <div class = "row">
        <div class = "col-sm-5">
            <h5>ログインID</h5>
        </div>
    	<div class = "col-sm-3">
        	<input type = "text"  name ="loginId" id ="inputLoginId" class ="form-control">
    	</div>
    </div>
    <br>
    <div class = "row">
        <div class = "col-sm-5">
            <h5>パスワード</h5>
        </div>
        <div class = "col-sm-3">
            <input type="password" name="password" id="inputPassword" class="form-control">
        </div>
    </div>
    <br>
    <br>
    <div class="submit">
    <button type="submit" class="btn btn-secondary btn-lg">ログイン</button>
    </div>
	</form>
	</body>
</html>

