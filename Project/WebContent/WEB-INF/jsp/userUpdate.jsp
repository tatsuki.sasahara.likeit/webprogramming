<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ユーザ情報更新</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="css/update.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class = "top" >
        <div class = "row">
            <div class ="col-sm-11">
                ${userInfo.name}さん&emsp;
            </div>
            <div class = "col-sm-1">
                <a href="LogOutServlet">ログアウト</a>
            </div>
        </div>
    </div>
    <br>
    <div class="title" align="center">
    <h1>ユーザ情報更新</h1>
    </div>
    <br>
    <c:if test="${errMsg != null}" >
    <div class="alert alert-danger" role="alert">
  		${errMsg}
	</div>
	</c:if>
    <br>
    <form class="form-signin" action="UserUpdateServlet" method="post">
    <input type = "hidden" name = "loginId" value = "${userUpdateInfo.loginId}">
    <div class ="data">
    <div class = "row">
        <div class = "col-sm-5">
            <h5>ログインID</h5>
        </div>
        <div class = "col-sm">
        	<div class="ID">
                ${userUpdateInfo.loginId}
            </div>
        </div>
    </div>

    <br>
    <div class = "row">
        <div class = "col-sm-5">
            <h5>パスワード</h5>
        </div>
        <div class = "col-sm">
            <input type="password" name = "password" id = "inputPassword" class = "form-signin">
        </div>
    </div>

    <br>
    <div class = "row">
        <div class = "col-sm-5">
            <h5>パスワード(確認)</h5>
        </div>
        <div class = "col-sm">
            <input type="Password" name = "checkPassword" id = "inputCheckPassword" class = "form-signin" >
        </div>
    </div>

    <br>
    <div class = "row">
        <div class = "col-sm-5">
            <h5>ユーザ名</h5>
        </div>
        <div class = "col-sm">
            <input type = "text"  name ="userName" id ="inputUserName" class ="form-signin" value = "${userUpdateInfo.name}">
        </div>
    </div>

     <br>
    <div class = "row">
        <div class = "col-sm-5">
            <h5>生年月日</h5>
        </div>
        <div class = "col-sm">
            <input type="date" name="birthDate" id = "inputBirthDate" class = "form-signin" max="2060-12-31" value = "${userUpdateInfo.birthDate}">
        </div>
    </div>
    </div>
    <br>
    <br>
    <br>
    <div class="button" align="center">
        <button type="submit" class="btn btn-secondary btn-lg">更新</button>
    </div>
    </form>
    <br>
    <div class ="back">
        <a href="UserListServlet">戻る</a>
    </div>
</body>
</html>