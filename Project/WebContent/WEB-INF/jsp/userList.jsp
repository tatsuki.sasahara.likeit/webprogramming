<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ユーザ一覧</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="css/userList.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class = "top" >
        <div class = "row">
            <div class ="col-sm-11">
                ${userInfo.name}さん&emsp;
            </div>
            <div class = "col-sm-1">
                <a href="LogOutServlet">ログアウト</a>
            </div>
        </div>
    </div>

    <br>
    <div class="title" align="center">
    <h1>ユーザ一覧</h1>
    </div>
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
    <br>
    <div class = "registration">
        <a href="UserCreateServlet">新規登録</a>
    </div>
    <br>
    <br>

    <div class ="data">
    <form method="post" action="#" class="form-horizontal">
        <div class = "row">
            <div class = "col-sm-5">
                <h5>ログインID</h5>
            </div>
            <div class = "col-sm-3">
                <input type = "text" name="loginId" id="login-id" class="form-control"/>
            </div>
        </div>
    <br>
        <div class = "row">
            <div class = "col-sm-5">
                <h5>ユーザ名</h5>
            </div>
            <div class = "col-sm-3">
                <input type="text" name="userName" id="userName" class="form-control"/>
            </div>
        </div>
    <br>
        <div class = "row">
            <div class = "col-sm-5">
                <h5>生年月日</h5>
            </div>
        <div class = "row">
            <div class = "col-sm">
                <input type="date" name="date-start" id="date-start" class="form-control" />
            </div>
            <div class="col-sm-1 text-center">
                ~
            </div>
            <div class="col-sm">
                <input type="date" name="date-end" id="date-end" class="form-control"/>
            </div>
        </div>
    </div>

    <br>
    <div class = "serch">
    	<button type="submit" class="btn btn-secondary btn-lg">検索</button>
    </div>
    </form>
    </div>
    <br>
    <br>

  <table class="table">

  <thead class="thead-light">
    <tr>
      <th scope="col">ログインID</th>
      <th scope="col">ユーザ名</th>
      <th scope="col">生年月日</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  <c:forEach var="user" items="${userList}" >
    <tr>
      <th scope="row">${user.loginId}</th>
      <td>${user.name}</td>
      <td>${user.birthDate}</td>
      <td>
        <div class = "select">
            <div class = "row">
                <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>&emsp;
                <c:if test = "${userInfo.name == user.name || userInfo.name == '管理者'}">
                	<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>&emsp;
                	<a class="btn btn-danger" href ="UserDropServlet?id=${user.id}">削除</a>
                </c:if>
              </div>
          </div>
        </td>
     </tr>
     </c:forEach>
  </tbody>

</table>
</body>
</html>

