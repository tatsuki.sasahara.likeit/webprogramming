<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ユーザ情報詳細</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="css/userData.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <div class = "top" >
        <div class = "row">
            <div class ="col-sm-11">
                ${userInfo.name}さん&emsp;
            </div>
            <div class = "col-sm-1">
                <a href="LogOutServlet">ログアウト</a>
            </div>
        </div>
    </div>
    <br>
    <div class="title" align="center">
    	<h1>ユーザ情報詳細参照</h1>
    </div>
    <br>
    <br>

    <div class ="data">
    <div class = "row">
        <div class = "col-sm-5">
            <h5>ログインID</h5>
        </div>
        <div class = "col-sm">
			<h5>${userDetail.loginId}</h5>
        </div>
    </div>
    <br>
    <div class = "row">
        <div class = "col-sm-5">
            <h5>ユーザ名</h5>
        </div>
        <div class = "col-sm">
            <h5>${userDetail.name}</h5>
        </div>
    </div>
    <br>
    <div class = "row">
        <div class = "col-sm-5">
            <h5>生年月日</h5>
        </div>
        <div class = "col-sm">
            <h5>${userDetail.birthDate}</h5>
        </div>
    </div>
    <br>
    <div class = "row">
        <div class = "col-sm-5">
            <h5>登録日時</h5>
        </div>
        <div class = "col-sm">
            <h5>${userDetail.createDate}</h5>
        </div>
    </div>
     <br>
    <div class = "row">
        <div class = "col-sm-5">
            <h5>更新日時</h5>
        </div>
        <div class = "col-sm">
            <h5>${userDetail.updateDate}</h5>
        </div>
    </div>
    </div>
    <br>
    <br>
    <div class ="back">
        <a href="UserListServlet">戻る</a>
    </div>
</body>
</html>

