<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ユーザ情報削除</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="css/drop.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class = "top" >
        <div class = "row">
            <div class ="col-sm-11">
                ${userInfo.name}さん&emsp;
            </div>
            <div class = "col-sm-1">
                <a href="LogOutServlet">ログアウト</a>
            </div>
        </div>
    </div>
    <br>
    <div class="title" align="center">
    	<h1>ユーザ削除確認</h1>
    </div>
    <br>

	<form class="form-signin" action="UserDropServlet" method="post">
	<input type = "hidden" name = "loginId" value = "${userInfo.loginId}">
    <div class ="text">
        <div class = "row">
            <h5>ログインID：${userInfo.loginId}</h5>
        </div>
        <div class ="row">
            <h5>を本当に削除してよろしいでしょうか。</h5>
        </div>
    </div>
    <br>
    <br>
    <div class="button" align="center">
    <div class ="row">
        <div class = "col-sm">
            <a href ="UserListServlet?id=${userInfo.loginId}" class="btn btn-secondary">キャンセル</a>
        </div>
        <div class = "col-sm">
            <button type="submit" class="btn btn-secondary">OK</button>
        </div>
    </div>
    </div>
    </form>
</body>
</html>

