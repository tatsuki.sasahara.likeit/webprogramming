package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッション切れ
		HttpSession session = request.getSession();

		if(session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		//idを引数にして、idに紐づくユーザ情報を出力する
		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User userUpdateInfo = userDao.findById(id);

		HttpSession sessionID = request.getSession();
		sessionID.setAttribute("userUpdateInfo",userUpdateInfo);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		String name = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");

		//エラー処理
	    if(!(password.equals(checkPassword))) {
	    	request.setAttribute("errMsg","入力された内容は正しくありません");

	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
	    	dispatcher.forward(request, response);
	    	return;

	    }else if(name.equals("") || birthDate.equals("")) {
	    	request.setAttribute("errMsg","入力された内容は正しくありません");

	    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
	    	dispatcher.forward(request, response);
	    	return;
	    }

	    //更新処理
	    UserDao userDao = new UserDao();
	    if(password.equals("") && checkPassword.equals("")){
	        userDao.userUpdateRestrictive(loginId,name,birthDate);
	    }else {
	    	userDao.userUpdateAll(loginId,password,name,birthDate);
	    }

	    response.sendRedirect("UserListServlet");
	}

}
