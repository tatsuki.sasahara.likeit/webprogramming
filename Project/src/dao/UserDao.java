package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public User findByLoginInfo(String loginId,String password) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			UserDao userDaoEn = new UserDao();
			String encodePass = userDaoEn.encode(password);

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, encodePass);
            ResultSet rs = pStmt.executeQuery();

            if(!rs.next()) {
            	return null;
            }

            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

		 } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
	        }
		}
	}

	public List<User>findAll(){
		Connection conn = null;
		List<User>userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user where id != 1";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
                userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(conn != null) {
				try {
					conn.close();
			}catch(SQLException e) {
				e.printStackTrace();
				return null;
				}
			}
		}
		return userList;
	}


	//登録処理
	public User userCreate(String loginId,String password,String userName,String birthDate) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			UserDao userDaoEn = new UserDao();
			String encodePass = userDaoEn.encode(password);

			String sql = "insert into user (login_id,password,name,birth_date) values(?,?,?,?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,loginId);
			pStmt.setString(2,encodePass);
			pStmt.setString(3,userName);
			pStmt.setString(4,birthDate);
			pStmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(conn != null) {
				try {
					conn.close();
			}catch(SQLException e) {
				e.printStackTrace();
				return null;
				}
			}
		}
		return null;
	}

	//ログインID重複判定用
	public User findSameLoginId(String loginId) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            ResultSet rs = pStmt.executeQuery();

            if(!rs.next()) {
            	return null;
            }

            String loginIdData = rs.getString("login_id");
            return new User(loginIdData);

		 } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
	        }
		}
	}

	//idに紐付く情報を返す
	public User findById(String id) {
        Connection conn = null;
        try {
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user WHERE id = ?";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, id);
            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return null;
            }

            String loginId = rs.getString("login_id");
            String name = rs.getString("name");
            Date birthDate = rs.getDate("birth_date");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            return new User(loginId,name,birthDate,createDate,updateDate);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	//更新処理 全て更新
	public User userUpdateAll(String loginId,String password,String userName,String birthDate) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			UserDao userDaoEn = new UserDao();
			String encodePass = userDaoEn.encode(password);

			String sql = "update user set password=?, name=?, birth_date=? where login_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,encodePass);
			pStmt.setString(2,userName);
			pStmt.setString(3,birthDate);
			pStmt.setString(4,loginId);
			pStmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	//パスワード以外の更新
	public User userUpdateRestrictive(String loginId,String userName,String birthDate) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "update user set name=?, birth_date=? where login_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,userName);
			pStmt.setString(2,birthDate);
			pStmt.setString(3,loginId);
			pStmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	//削除処理
	public User userDrop(String loginId) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "delete from user where login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,loginId);
			pStmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(conn != null) {
				try {
					conn.close();
			}catch(SQLException e) {
				e.printStackTrace();
				return null;
				}
			}
		}
		return null;
	}

	//暗号化
	public String encode(String password){
		try {
			String source = password;
			Charset charset = StandardCharsets.UTF_8;
			String algorithm = "MD5";
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);

			return result;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	//検索機能
	public List<User>SerchUser(String loginId ,String userName ,String dateStart ,String dateEnd){
		Connection conn = null;
		List<User>userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user where id != 1";

			if(!(userName.equals(""))) {
				sql += " AND name LIKE '%" + userName + "%'";
			}
			if(!(loginId.equals(""))) {
				sql += " AND login_id = '" + loginId + "'";
			}
			if(!(dateStart.equals(""))) {
				sql += " AND birth_date >= '" + dateStart +"'";
			}
			if(!(dateEnd.equals(""))) {
				sql += " AND birth_date <= '" + dateEnd +"'";
			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int id = rs.getInt("id");
                String loginId1 = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId1, name, birthDate, password, createDate, updateDate);
                userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(conn != null) {
				try {
					conn.close();
			}catch(SQLException e) {
				e.printStackTrace();
				return null;
				}
			}
		}
		return userList;
	}

}
